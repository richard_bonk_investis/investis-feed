<?php
/**
 * Plugin Name: Investis RSS Feed
 * Description: An Investis Digital plugin to enable your website.
 * Version: 1.0.1
 * Author: Investis Digital
 * Author URI: https://investisdigital.com
 */


function invd_init_feed(){
    if (isset($_REQUEST['feed'])) {
        if($_REQUEST['feed'] != '' ){
            $params = array(
                'post_type' => $_REQUEST['feed'],
            );
        } else {
            $params = array(
                'post_type' => 'post',
            );
        }
        invd_create_feed($params);
    }
    
}

function invd_create_feed($params){
    $defaults = array(
        'orderby' => 'date',
        'order'   => 'DESC',
        'posts_per_page' => '10',
    );
    $args = wp_parse_args( $params, $defaults );
    $feed_query = new WP_Query($args);
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
    header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
    echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
    ?>
<rss version="2.0"
xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:wfw="http://wellformedweb.org/CommentAPI/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:atom="http://www.w3.org/2005/Atom"
xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
>
<channel>
        <title><?php bloginfo_rss('name'); ?> - Feed</title>
        <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
        <link><?php bloginfo_rss('url') ?></link>
        <image><?php echo $image[0]; ?></image>
        <description><?php bloginfo_rss('description') ?></description>
        <lastBuildDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_lastpostmodified('GMT'), false); ?></lastBuildDate>
        <language><?php echo get_option('rss_language'); ?></language>
        <sy:updatePeriod><?php echo apply_filters( 'rss_update_period', 'daily' ); ?></sy:updatePeriod>
        <sy:updateFrequency><?php echo apply_filters( 'rss_update_frequency', '1' ); ?></sy:updateFrequency>
        <?php while($feed_query->have_posts()) : $feed_query->the_post(); ?>
                <?php 
                $item_date = get_post_meta($feed_query->post->ID,'event_start_date', true);
                $item_time = get_post_meta($feed_query->post->ID,'event_start_time', true);
                if($item_time != ""){$item_date = $item_date . $item_time;}
                ?>
                <item>
                        <title><![CDATA[<?php the_title_rss(); ?>]]></title>
                        <link><?php the_permalink_rss(); ?></link>
                        <pubDate><?php echo mysql2date('D, d M Y H:i:s', $item_date , false); ?></pubDate>
                        <publishDate><?php echo mysql2date('D, d M Y H:i:s', get_post_time('Y-m-d H:i:s', true), false); ?></publishDate>
                        <guid isPermaLink="false"><?php the_guid(); ?></guid>
                </item>
        <?php endwhile; ?>
</channel>
</rss>
<?php die();
}

add_action('init', 'invd_init_feed');
