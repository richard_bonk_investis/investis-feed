Once activated the usage is as follows:

- add the following url in browser:
?feed - this will show the 10 latest 'post' items

- to use custom post type:
?feed=event - will show the 10 lastest 'event' post type items

e.g. http://site.name/?feed=event
